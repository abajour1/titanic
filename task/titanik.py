import pandas as pd

def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    df['Title'] = df['Name'].str.extract(r',\s(.*?\.).*')
    mr_age = df[df['Title']=='Mr.']['Age'].median()
    mr_loc = df.loc[df['Title']=='Mr.', 'Age']
    mr_missing = df[df['Title']=='Mr.']['Age'].isnull().sum()
    mrs_age = df[df['Title']=='Mrs.']['Age'].median()
    mrs_loc = df.loc[df['Title']=='Mrs.', 'Age']
    mrs_missing = df[df['Title']=='Mrs.']['Age'].isnull().sum()
    miss_age = df[df['Title']=='Miss.']['Age'].median()
    miss_loc = df.loc[df['Title']=='Miss.', 'Age']
    miss_missing = df[df['Title']=='Miss.']['Age'].isnull().sum()
    mr_loc.fillna(mr_age)
    mrs_loc.fillna(mrs_age)
    miss_loc.fillna(miss_age)
    res = [('Mr.', mr_missing, round(mr_age)), ('Mrs.', mrs_missing, round(mrs_age)), ('Miss.', miss_missing, round(miss_age))]
    return res
